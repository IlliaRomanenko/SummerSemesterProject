using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*public abstract class Animals
{
    public abstract int LegCount();

}

public class Mammal : Animals
{
    private bool _hasFur;
    protected int _legCount;
    private int _numberOfEars;
    public Mammal()
    {
        _legCount = 4;
        _numberOfEars = 2;
        _hasFur = false;
    }

    public Mammal(int newLegCount)
    {
        _legCount = newLegCount;
        _numberOfEars = 2;
        _hasFur = false;
    }

    public Mammal(int newLegCount, bool hasFur)
    {
        _legCount = newLegCount;
        _numberOfEars = 2;
        _hasFur = false;
    }


    public override int LegCount()
    {
        return _legCount;
    }

    // setter: sets the value to a local variable
    public void SetNumberOfEras(int newNumberOfEras)
    {
        if ((newNumberOfEras > 0) && (newNumberOfEras < 5)){
            _numberOfEars = newNumberOfEras;
        }

    }

    // setter: sets the value to a local variable
    public void SetFur(bool newFur)
    {
        _hasFur = newFur;

    }

    // Getter function
    public bool GetFur()
    {
        return _hasFur;
    }
}

public class Insects : Animals
{
    public Insects()
    {

    }
    public override int LegCount()
    {
        return 6;
    }

}

public class Kangoo : Mammal
{
    public Kangoo()
    {
        this.SetFur(true);
        this._legCount = 2;
    }

    public Kangoo(int legCount)
    {
        this.SetFur(true);
        this._legCount = legCount;
    }

    public Kangoo(int legCount, bool hasFur)
    {
        this.SetFur(hasFur);
        this._legCount = legCount;
    }

}
*/