using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCScript : MonoBehaviour
{
    // Start is called before the first frame update
    private int IdleHash = Animator.StringToHash("Idle");
    private int inTriggerEventrHash = Animator.StringToHash("inTriggerEvent");
    private Animator animator;

    void Start()
    {
        animator = GetComponent<Animator>();

    }

    void OnTriggerStay() {

        animator.SetTrigger(inTriggerEventrHash);

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
