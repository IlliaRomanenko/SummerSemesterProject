﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatePlane : MonoBehaviour
{
	public float horizontalSpeed = 0.5F;
	public float verticalSpeed = 0.5F;

	// Update is called once per frame
	void Update ()
	{
		float h = horizontalSpeed * Input.GetAxis ("Mouse X");
		// * -1.0F to flip the direction
		float v = -1.0F * verticalSpeed * Input.GetAxis ("Mouse Y");
		// perform rotation in x,z and NOT in y
		transform.Rotate (v, 0, h);
	}
}