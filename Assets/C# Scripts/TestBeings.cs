using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestBeings : MonoBehaviour
{
    Tanuki tanuki;
    CharacterEnemy characterEnemy;
    CharacterSamurai characterSamurai;

    void Start()
    {
        characterEnemy = new CharacterEnemy(1, 400f);
        tanuki = new Tanuki();
        characterSamurai = new CharacterSamurai(100f);

        Debug.Log("CharacterSamurai has " + characterSamurai.SetHealth() + " HP");
        Debug.Log("There is " + tanuki.Number() + " Tanuki");
        Debug.Log("There is " + characterEnemy.Number() + " with " + characterEnemy.SetHealth() + " HP");
    }
}
