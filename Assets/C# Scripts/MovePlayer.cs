﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class MovePlayer : MonoBehaviour {

    private int IdleHash = Animator.StringToHash("Idle"); // The string to convert to Id.
    private int WalkHash = Animator.StringToHash("Forwards");
    private int BackWalkHash = Animator.StringToHash("Backwards");

    // Parameter Hashs:
    private int SpeedHash = Animator.StringToHash("Speed");
    private int RunHash = Animator.StringToHash("RunForwards");
    private int IsSprintHash = Animator.StringToHash("IsSprint");
    private int IsLootHash = Animator.StringToHash("isLoot");
    private int IsWaveHash = Animator.StringToHash("isWave");

    private Animator animator;
    private float speedup = 2.0F;
    public float rotationSpeed = 50.0F;
	public float movementSpeed = 1.0F;
    public Vector3 jump;
    public float jumpForce = 3.0f;
    public float forwardForce = 2.0f;
    public bool isGrounded;
    private bool isShiftDown;
    Rigidbody rb;
    void Start()
    {

        rb = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
        jump = new Vector3(0.0f, 1.0f, 0.0f);
    }
    void OnCollisionStay(Collision collision)
    {

        if (collision.gameObject.CompareTag("Ground"))
        {
            animator.SetBool("isGround", true);
            isGrounded = true;
        }
    }

    // Update is called once per frame
    void Update () {
        {
            float z = 0.0f;

            z = Input.GetAxis("Vertical");
            //float x = Input.GetAxis("Horizontal");
            float h = Input.GetAxis("Mouse X") * rotationSpeed;
            /*Vector3 direction = new Vector3(z,0f,h).normalized;
            if (direction.magnitude >= 0.1f) 
            { 
                float targetAngle = Mathf.Atan2(direction.x,direction.y)*Mathf.Rad2Deg;

                transform.rotation = Quaternion.Euler(0f, angle, 0f);
            }*/
            AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);

            animator.SetFloat(SpeedHash, z);

            if (Input.GetKeyDown(KeyCode.LeftShift) || Input.GetKeyDown(KeyCode.RightShift)) isShiftDown = true;
            if (Input.GetKeyUp(KeyCode.LeftShift) || Input.GetKeyUp(KeyCode.RightShift)) isShiftDown = false;
            // inform animator if running
            animator.SetBool(IsSprintHash, isShiftDown);
            // By default, Input.GetAxis(“Horizontal”) and Input.GetAxis(“Vertical”) allow the player to use the WASD 
            // and arrow keys, a controller pad or other device to move the player (see InputManager).
            // Returns the value of the virtual axis identified by axisName.
            // The value will be in the range -1...1 for keyboard and joystick input. If the axis is setup to be delta mouse movement, 
            // the mouse delta is multiplied by the axis sensitivity and the range is not -1...1.

            //	var x = Input.GetAxis ("Horizontal") * Time.deltaTime * 50.0f;

            if (Input.GetKey(KeyCode.Space) && isGrounded)
            {

                rb.AddForce(jump * jumpForce, ForceMode.Impulse);
                animator.Play("Jump");
                isGrounded = false;
                animator.SetBool("isGround", false);


            }
            else if (!isGrounded) {
                if (!isShiftDown) z = z * Time.deltaTime * movementSpeed;
                else z = z * Time.deltaTime * 3;
                // we multiply here, because otherwise the value would be too small for the condition of the transition
                //x *= Time.deltaTime * movementSpeed;
                transform.Translate(0, 0, z);
            }
            if (Input.GetKeyDown(KeyCode.T) && stateInfo.shortNameHash == IdleHash) animator.SetTrigger(IsWaveHash);


            if ((stateInfo.shortNameHash == WalkHash) || (stateInfo.shortNameHash == BackWalkHash)
                || (stateInfo.shortNameHash == RunHash) || !isGrounded)
            {
                if (!isShiftDown) z = z * Time.deltaTime * movementSpeed;
                else z = z * Time.deltaTime * speedup;
                // we multiply here, because otherwise the value would be too small for the condition of the transition
                //x *= Time.deltaTime * movementSpeed;
                transform.Translate(0, 0, z); // transform the z-postion controlled by keyboard

            }
            if (Input.GetKeyDown(KeyCode.P) && (stateInfo.shortNameHash == IdleHash))
            {
                animator.SetTrigger(IsLootHash);
            }

            h *= Time.deltaTime;

            transform.Rotate(0, h, 0); // perform rotation controlled by mouse

			//transform.Translate (x, 0, z); // transform the z-postion controlled by keyboard
		}
	}	
}
