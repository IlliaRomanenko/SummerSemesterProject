﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickObject : StateMachineBehaviour
{
    public float PickRadius = 0.6f;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

        // what is the current position of my Player? (parent of the animator component)
        Vector3 center = animator.GetComponentInParent<Transform>().position;
        // check if there is a collsion with a collider. Object to pick must have a collider to be detected!
        Collider[] hitColliders = Physics.OverlapSphere(center, PickRadius);

        int i = 0;

        while (i < hitColliders.Length)
        {
            // was there a hit? Loop over all components witch are intersecting
            animator.SetBool("IsFail", true);

            // is there a script attached (only our pickable prefabs would contain one). You can also check on a tag...
            LootedEvents pickscript = hitColliders[i].GetComponent<LootedEvents>();

            if (pickscript != null)
            {
                animator.SetBool("IsFail", false);
                // if script was found, then execute it
                pickscript.DoLootObject(hitColliders[i]);
                break;
            }

            i++;
        }
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
  ///  override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
   // {


//    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
