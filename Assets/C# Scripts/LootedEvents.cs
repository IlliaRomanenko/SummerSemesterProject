﻿/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///  Looted sctipt with events and icon
///  
///  Prof. Dr. Frank Gabler
///  Last change: 11.06.24
///
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; // we need that for the Icon

public class LootedEvents : MonoBehaviour
{
    private float speed;
    private Vector3 startScale;
    public float delta = 0.01f;

    public GameEvent dropped;
    public GameEvent picked;

    private AudioSource audio;
    public AudioClip clip;


    public Image Icon;

    // Start is called before the first frame update
    void Start()
    {
        audio = GetComponent<AudioSource>();

        // get the current scale
        startScale = transform.localScale;
        // generate a random speed for the animation
        speed = Random.Range(5.0f, 10.0f);

        dropped.Occurred(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        // let the scale change to animate carrot
        Vector3 v = startScale;
        // do some simple oszillations by sine-function
        v.y += delta * Mathf.Sin(Time.time * speed);
        this.transform.localScale = v;
    }

    public void DoLootObject(Collider thisCollider)
    {
        GameObject obj = this.gameObject;

        if (obj != null)
        {
            // play jingle
            audio.PlayOneShot(clip, 1.0f);
            // hide it
            this.gameObject.GetComponent<MeshRenderer>().enabled = false;
            this.gameObject.GetComponent<Collider>().enabled = false;
            // fire event to inform listeners that the item is gone now
            picked.Occurred(obj);
            // destory it with a little delay - otherwise it can not play soundfile :-)
            Destroy(obj, 2.5f);

            // Update Singleton about colleced item
            GameHandler24.Instance.IncNumberOfCollectedItems();

        }

    }
}
