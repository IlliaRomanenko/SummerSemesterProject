using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public abstract class Beings
{
    public abstract int Number();
    public abstract float SetHealth();
}

public class People : Beings
{
    protected int _Number;
    protected float Health;

    public override int Number()
    {
        return _Number;
    }

    public override float SetHealth()
    {   
        return Health;
    }
}

public class Animals : Beings
{
    protected int _Number;
    protected int Health;

    public override int Number()
    {
        return _Number;
    }
    public override float SetHealth()
    {
        return Health;
    }
}

public class CharacterSamurai : People
{
    public CharacterSamurai(float numberhealth)
    {
        this.Health = numberhealth;
    }
    public override float SetHealth()
    {
        return Health;
    }
}

public class CharacterEnemy : People
{
    public CharacterEnemy(int NumberOfThem, float numberhealth)
    {
        this._Number = NumberOfThem;
        this.Health = numberhealth;
    }


    public override int Number()
    {
        return _Number;
    }
    public override float SetHealth()
    {
        return Health;
    }
}

public class Tanuki : Animals
{
    public override int Number()
    {
        return 1;
    }
}
