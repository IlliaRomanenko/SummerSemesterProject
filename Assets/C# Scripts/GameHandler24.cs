
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public sealed class GameHandler24 : MonoBehaviour
{
    public GameObject ObjToSpawn;
    public GameObject SecObjToSpawn;
    public static GameHandler24 Instance { get; set; }
    public int NorOfObjectToSpawn =0;
    public int NorOfSecObjectToSpawn = 0;
    private int NumberOfCollectedItems;

    private int terrainWidth;
    private int terrainLength;
    private int terrainPosX;
    private int terrainPosZ;
    private Terrain terrain;
    private void Awake() {
        if (Instance == null) {
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        terrain = GameObject.Find("Terrain").GetComponent<Terrain>();
        if (terrain != null) 
        {
            terrainWidth = (int)terrain.terrainData.size.x;
            terrainLength = (int)terrain.terrainData.size.z;
            terrainPosX = (int)terrain.transform.position.x;
            terrainPosZ = (int)terrain.transform.position.z;
        }
        else 
        {
            Debug.LogError("Error: No Terrain Found");
        }
        SpawnObjects(ObjToSpawn);
    }
    private void SpawnObjects(GameObject newObj) 
    {
        if (newObj != null) 
        {
            for (int objNumber =0; objNumber < NorOfObjectToSpawn; objNumber++) 
            {
                int posx = Random.Range(terrainPosX, terrainWidth + terrainPosX);
                int posz = Random.Range(terrainPosZ, terrainLength + terrainPosZ);
                float posy = Terrain.activeTerrain.SampleHeight(new Vector3(posx,0,posz));
                GameObject obj = (GameObject)Instantiate(newObj, new Vector3(posx, posy, posz), Quaternion.identity);
            }
        }
    
    }
    public void IncNumberOfCollectedItems()
    {
        NumberOfCollectedItems++;
        Debug.Log("New Item found #" + NumberOfCollectedItems.ToString());
    }
    public void UpdateUI() {
        TMP_Text outtext = GameObject.Find("Canvas/UITextField").GetComponent<TMP_Text>();
        if (outtext != null)
        {
            outtext.text = "Found: " + NumberOfCollectedItems.ToString();

        }
    }
    // Update is called once per frame
    void Update()
    {
        UpdateUI();


    }
}
