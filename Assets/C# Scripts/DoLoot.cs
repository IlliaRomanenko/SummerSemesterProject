using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoLoot : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void DoLootObject(Collider thisCollider) 
    { 
        GameObject obj = thisCollider.gameObject;
        if (obj != null)
        {
            Destroy(obj, 0.1f);

        }
        GameHandler24.Instance.IncNumberOfCollectedItems();
    }
}
