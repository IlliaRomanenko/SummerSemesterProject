using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackPlayer : MonoBehaviour
{
    private int FastAttackHash = Animator.StringToHash("FastAttack");
    private int StrongAttackHash = Animator.StringToHash("StrongAttack");

    // Parameter Hashs:
    private int IsFastAttackHash = Animator.StringToHash("isFastAttack");
    private int IsStrongAttackHash = Animator.StringToHash("isStrongAttack");
    private int IsKatanaEnableAttackHash = Animator.StringToHash("isKatanaEnable");

    public GameObject katana;
    private Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);

        if (Input.GetKeyDown(KeyCode.Alpha1) && !katana.activeSelf) {
            animator.SetBool(IsKatanaEnableAttackHash, true);
            katana.SetActive(true); }
        else if (Input.GetKeyDown(KeyCode.Alpha1) && katana.activeSelf)
        {
            animator.SetBool(IsKatanaEnableAttackHash, false);

            katana.SetActive(false);
        }
        if (Input.GetMouseButtonDown(0) && stateInfo.shortNameHash != FastAttackHash && katana.activeSelf)
        {
            //animator.SetTrigger(IsFastAttackHash);
            animator.Play("FastAttack");
        }
        if (Input.GetMouseButtonDown(1) && stateInfo.shortNameHash != StrongAttackHash && katana.activeSelf)
        {
            //animator.SetTrigger(IsStrongAttackHash);
            animator.Play("StrongAttack");
        }
    }
}
