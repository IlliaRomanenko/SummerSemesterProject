using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Functions1 : MonoBehaviour
{
    private int a = 10;
    private int b = 20;

    // Start is called before the first frame update
    void Start()
    {
        int c = AddTwoNumbers(a,b);
        Debug.Log(c);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // function MUSST be outside of Setup and Update but within Class
    int AddTwoNumbers(int a, int b) {
        return a + b;
    }
}
