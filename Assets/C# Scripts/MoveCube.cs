﻿//---------------------------------------------------------------------------------------------
// Prof. Dr. Frank Gabler, University of Applied Sciences Darmstadt, Unity3D Elective WS17/18
// Created: 20-11-17
// Last Update: 20-11-17
//---------------------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// MonoBehaviour is the base class from which every Unity script derives.
// When you use C#, you must explicitly derive from MonoBehaviour.
// https://docs.unity3d.com/ScriptReference/MonoBehaviour.html

public class MoveCube : MonoBehaviour
{
	private Animator animator;
	public float speed = 0.5f;
	public float spacing = 0.1f;
    public float horizontalSpeed = 0.5F;


    // Vector3 : This structure is used throughout Unity to pass 3D positions and directions around.
    // It also contains functions for doing common vector operations.
    private Vector3 pos;


	// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	void Start ()
	{	
		animator = GetComponent<Animator>();
		pos = transform.position;
	}

	//    Update is called every frame, if the MonoBehaviour is enabled.
	// Update is the most commonly used function to implement any kind of game behaviour.

	void Update ()
	{

		if (Input.GetKey(KeyCode.W))
		{
			animator.SetBool("isMoveForward", true);
			//pos.z += spacing;
			transform.position += transform.forward * speed * Time.deltaTime;
		}
		else { animator.SetBool("isMoveForward", false); }

		if (Input.GetKey(KeyCode.S))
		{
			animator.SetBool("isMoveBackward", true);
			//pos.z -= spacing;
			transform.position -= transform.forward * speed * Time.deltaTime;
		}
		else { animator.SetBool("isMoveBackward", false); }

		if (Input.GetKey(KeyCode.A))
		{
			animator.SetBool("isMoveLeft", true);
			//pos.x -= spacing;
			transform.position -= transform.right * speed * Time.deltaTime;
		}
		else { animator.SetBool("isMoveLeft", false); }

		if (Input.GetKey(KeyCode.D))
		{
			animator.SetBool("isMoveRight", true);
			//pos.x += spacing;transform.position += transform.forward * speed * Time.deltaTime;
			transform.position += transform.right * speed * Time.deltaTime;
		}
		else { animator.SetBool("isMoveRight", false); }
        float h = horizontalSpeed * Input.GetAxis("Mouse X");
        // * -1.0F to flip the direction
        transform.Rotate(0, h, 0);


        // Vector3.MoveTowards(transform.position, target.position, step); Moves a point current in a straight line towards a target point.
        //transform.position = Vector3.MoveTowards (transform.position, pos, speed * Time.deltaTime);

		// Time.deltaTime:
		// The time in seconds it took to complete the last frame (Read Only).
		// Use this function to make your game frame rate independent.
		// If you add or subtract to a value every frame chances are you should multiply with Time.deltaTime. 
		// When you multiply with Time.deltaTime you essentially express: I want to move this object 10 meters per second instead of 10 meters per frame.

	}
	
}