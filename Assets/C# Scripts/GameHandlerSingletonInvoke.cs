/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///  GameHandler/Spawner based on simple Sigleton-Pattern
///  Two modes: all at once, invoke repeating
///  Prof. Dr. Frank Gabler
///  Last change: 11.06.24
///
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;
using TMPro; // include TextMeshPro

public sealed class GameHandlerSingletonInvoke : MonoBehaviour
{
    private int NumberOfCollectedItems;
    //    private bool GateKeyFound = false;

    private int terrainWidth;
    // terrain size (x)
    private int terrainLength;
    // terrain size (z)
    private int terrainPosX;
    // terrain position x
    private int terrainPosZ;
    // Terrain
    private Terrain terrain;

    public GameObject objectToPlace;
    // public GameObject enemiesToPlace;

    public int NrOfObjectsToSpawn = 0;

    public bool DoInvokeSpawn = false;

    public GameEvent ItemDropped;

    public static GameHandlerSingletonInvoke Instance { set; get; }

    //  public bool hasKey() { return GateKeyFound; }

    // Check early in the game loop if instance is already there
    private void Awake()
    {
        // check if instance is already there...
        if (Instance == null)
        {
            // this = reference to the object itself. Hand over reference to instance
            Instance = this;
            // Do not destroy it if another scene is loaded. Keep it till the end of the application!
            DontDestroyOnLoad(this.gameObject); // defined in MonoBehavior

            NumberOfCollectedItems = 0;
            UpdateUI();
        }
        else
        {
            // if one instance is already existing then destory the new one: thre is only ONE!
            Destroy(this.gameObject); // defined in MonoBehavior
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        //        SceneManager.activeSceneChanged += ChangedActiveScene; // Deligate

        // search the Terrain and store the reference to read the dimensions
        terrain = GameObject.Find("Terrain").GetComponent<Terrain>();
        // check first if reference is valid, otherwise is will crash by access violaton 
        if (terrain != null)
        {
            terrainWidth = (int)terrain.terrainData.size.x;
            // terrain size z
            terrainLength = (int)terrain.terrainData.size.z;
            // terrain x position
            terrainPosX = (int)terrain.transform.position.x;
            // terrain z position
            terrainPosZ = (int)terrain.transform.position.z;
        }
        else
        {
            // no Terrain found! Bad luck... raise and ERROR!
            Debug.LogError("No Terrain found!");
        }

        if (!DoInvokeSpawn)
        {
            SpawnObjects(); // crate all objects at once
        }
        else
        {
            // start to create eggs after 5 seconds and drop one every second
            InvokeRepeating("CreateEgg", 5, 1);

        }
    }

    void SpawnObjects()
    {
        // call the function to instantiate a prefab as often as necessary 
        for (int oNumber = 0; oNumber < NrOfObjectsToSpawn; oNumber++)
        {
            CreateEgg();
        }
    }

    // because InvokeRepeating does not accept functions with parameters!
    private void CreateEgg()
    {
        // generate random x position
        int posx = Random.Range(terrainPosX, terrainPosX + terrainWidth);
        // generate random z position
        int posz = Random.Range(terrainPosZ, terrainPosZ + terrainLength);

        Vector3 pos = new Vector3(posx, 0, posz);
        // get the terrain height at the random position
        pos.y = Terrain.activeTerrain.SampleHeight(pos);

        // check first if reference is valid, otherwise is will crash by access violaton 
        if (objectToPlace != null)
        {
            GameObject newObject = (GameObject)Instantiate(objectToPlace, pos, Quaternion.identity); // defined in MonoBehavior
            // There was a new object instantiated -> inform all the listeners
            ItemDropped.Occurred(newObject);
        }
    }

    public void IncNumberOfCollectedItems()
    {
        NumberOfCollectedItems++;
        Debug.Log("New item found #" + NumberOfCollectedItems.ToString());
        UpdateUI();
    }

    public void UpdateUI()
    {
        TMP_Text outtext = GameObject.Find("Canvas/NumberOfFoundItemsText").GetComponent<TMP_Text>();
        if (outtext != null)
            outtext.text = "Found:" + NumberOfCollectedItems.ToString();

        // show the Key Image if Key was found, otherwise hidden.
        //    GameObject.Find("BackImageKey").GetComponent<Image>().enabled = GateKeyFound;
    }

    // Eventhandler on activeSceneChanged (Deligate)
    private void ChangedActiveScene(Scene current, Scene next)
    {
        UpdateUI();
        SpawnObjects();
    }

    // Update is called once per frame
    void Update()
    {
        // if "N" is pressed, then just reload the main scene
        if (Input.GetKeyDown(KeyCode.N))
        {
            SceneManager.LoadScene("Samu1", LoadSceneMode.Single);
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            UniStorm.UniStormManager.Instance.RandomWeather();
        }
    }
}
*/