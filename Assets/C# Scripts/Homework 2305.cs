using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Homework2305 : MonoBehaviour
{
    private float[] RandomArray = new float[10]; 

    void Start()
    {
        for (int i =0; i< RandomArray.Length; i++)
        {
            RandomArray[i] = Random.Range(1.0f,10.0f);
        }
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("Average value is " + averagevalue(RandomArray));
    }
    float averagevalue(float[] arr)
    {
        float averagevalue = 0f;
        foreach (float v in arr)
        {
            averagevalue += v;
        }
        return averagevalue / arr.Length; ;
    }
}
